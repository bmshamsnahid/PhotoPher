var express = require('express');
var router = express.Router();
var crudFeedback = require('../../controller/feedbackController');

router.post('/feedback',isLoggedIn,crudFeedback.addFeedback);
router.get('/feedback',isLoggedIn,crudFeedback.allFeedback);
router.get('/feedback/:id',isLoggedIn,crudFeedback.findSingleFeedback);
router.put('/feedback/:id',isLoggedIn,crudFeedback.editFeedback);
router.delete('/feedback/:id',isLoggedIn,crudFeedback.deletFeedback);


function isLoggedIn(req,res,next) {
    if(req.user)
        next();
    else {
        res.status(200).json({
            'message':'Authentication failed'
        })
    }
}


module.exports = router;