var express = require('express');
var router = express.Router();
var crudMessage = require('../../controller/messageController');


router.post('/message',isLoggedIn,crudMessage.addMessage);
router.get('/message',isLoggedIn,crudMessage.allMessage);
router.get('/message/:id',isLoggedIn,crudMessage.findSingleMessage);
router.patch('/message/:id',isLoggedIn,crudMessage.editMessage);
router.delete('/message/:id',isLoggedIn,crudMessage.deletMessage);

function isLoggedIn(req,res,next) {
    if(req.user)
        next();
    else {
        res.status(200).json({
            'message':'Authentication failed'
        })
    }
}

module.exports = router;