var express = require('express');
var router = express.Router();
var multer = require('multer');
var fs = require("fs");
var controllerAccess = require('../../controller/photoController');

//multer config.
var storage = multer.diskStorage({  // setting file path
    destination: (req, file, callback) => {
        callback(null, 'public/uploads');
    },
    filename: (req, file, callback) => {  // setting file name
        callback(null, Date.now() + "-" + file.originalname );
    }
});

// set fieldname 'userPhoto'
var upload = multer({ storage : storage}).single('userPhoto');

//photo routes
router.post('/photo', isLoggedIn, upload, controllerAccess.addPhoto);
router.get('/photo', isLoggedIn, controllerAccess.getAllPhoto);
router.get('/photo/:id', isLoggedIn, controllerAccess.getAPhoto);
router.delete('/photo/:id', isLoggedIn, controllerAccess.deleteAPhoto);
router.patch('/photo/:id', isLoggedIn, upload, controllerAccess.updatePhoto);

module.exports = router;

function isLoggedIn(req, res, next) {
    if(req.user) {
        return next();
    } else {
        res.status(200).json({
            'message': 'Authentication failed'
        });
    }
}