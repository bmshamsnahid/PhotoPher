var express = require('express');
var router = express.Router();
var multer = require('multer');
var fs = require('fs');

var photographerController = require('../../controller/photographerController');

//multer storage configaration
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        var filePath = '/public/PhotographerImage/profilePhoto-' + req.params.id;
        fs.access(filePath, error => {
            if (!error) {
                fs.unlink(filePath,function(error){
                    console.log(error);
                });
            } else {
                console.log(error);
            }
            cb(null, 'public/PhotographerImage');
        });
    },
    filename: function (req, file, cb) {
        console.log('id: ' + req.params.id);
        cb(null, 'profilePhoto-' + req.params.id);
    }
});
var upload = multer({ storage: storage });

router.post('/photographer', photographerController.createPhotographer);
router.get('/photographer', photographerController.getAllPhotographer);
router.get('/photographer/:id', photographerController.getSinglePhotographer);
router.put('/photographer/:id', upload.single('image'), photographerController.updatePhotographer);
router.delete('/photographer/:id', photographerController.deletePhotographer);

module.exports = router;