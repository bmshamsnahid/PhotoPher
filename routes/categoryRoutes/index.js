var express = require('express');
var router = express.Router();
var categoryController = require('../../controller/categoryController');

router.post('/category', isLoggedIn, categoryController.createCategory);
router.get('/category', isLoggedIn, categoryController.findAllCategory);
router.get('/category/:id', isLoggedIn, categoryController.findSingleCategory);
router.patch('/category/:id', isLoggedIn, categoryController.updateCategory);
router.delete('/category/:id', isLoggedIn, categoryController.deleteCategory);

module.exports = router;

function isLoggedIn(req, res, next) {
    if(req.user) {
        return next();
    } else {
        res.status(200).json({
            'message': 'Authentication failed'
        });
    }
}