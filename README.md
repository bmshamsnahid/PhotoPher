# Photographer Portfolio

## Instructions

In order to download the code and try it for yourself:

1. Clone the repo: `git clone https://bmshamsnahid@bitbucket.org/bmshamsnahid/photographer-portfolio.git`
2. Install packages: `npm install`
3. Change out the database configuration in config/database.js
4. Change out auth keys in config/auth.js
5. Launch: `node server.js` or just `nodemon`
6. Visit in your browser at: `http://localhost:8080`


Hello
