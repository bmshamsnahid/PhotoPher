var assert = require('assert');

var uploadPhoto = (db, docs, callback) => {
    db.collection('photos').insertOne(docs, (err, r) => {
        assert.equal(null, err);
        db.close();
        callback(docs);
    });
};

var viewAllPhoto = (db, callback) => {
    var collection = db.collection('photos');
    collection.find({}).toArray(function(err, docs) {
        assert.equal(err, null);
        callback(docs);
    });
};

var viewAPhoto = (db, id, callback) => {
    var collection = db.collection('photos');
    db.collection("photos").findOne({'_id': id}, function(err, docs) {
        assert.equal(err, null);
        callback(docs);
    });
};


var deletePhoto = (db, id, callback) => {
    var collection = db.collection('photos');

    collection.deleteOne({'_id': id},(err, docs) => {
        assert.equal(err, null);
        callback(docs);
    });
};


var updatePhoto = (db, id, docs, callback) => {
    var collection = db.collection('photos');

    collection.updateOne({'_id': id}, docs, (err, result) => {
        assert.equal(err, null);
        callback(result);
    });
};


module.exports = {
    uploadPhoto,
    viewAllPhoto,
    viewAPhoto,
    deletePhoto,
    updatePhoto
};