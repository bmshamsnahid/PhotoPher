var assert = require('assert');

var insertMessage = (db,task,callback)=> {
    var collection = db.collection('message');

    collection.insertOne(task,function (err,result) {
        assert.equal(err,null);
        //console.log("Inserted plus 1 to in the collection.");
        callback(result);

    })
}

var findMessages = (db,callback)=> {

    var collection = db.collection('message');
    collection.find({}).toArray((err,docs)=>{
        assert.equal(err,null);
        callback(docs);
    })

}

var findMessage = function (db,id,callback) {
    var collection = db.collection('message');
    collection.find(id).toArray((err,docs)=> {
        assert.equal(err,null);
        //console.log("Found the following records");
        //console.log(docs);
        callback(docs);

    })
}


var updateMessage = (db,id,docs,callback)=> {

    var collection = db.collection('message');

    var myId = {
        '_id':id
    }

    collection.updateOne(myId, docs, (err,result)=> {
        assert.equal(err,null);

        callback(result);
    })


}

var removeMessage =  (db,id,callback)=> {

    var collection = db.collection('message');

    var myId = {
        '_id':id
    }

    collection.deleteOne(myId,(err,result)=>{
        assert.equal(err,null);
        callback(result);
    })

}

module.exports = {
    insertMessage,
    findMessage,
    findMessages,
    removeMessage,
    updateMessage
}