var assert = require('assert');

var createPhotographer = (db, docs, callback) => {
    
    db.collection('photographers').insertOne(docs, (err, r) => {
        assert.equal(null, err);
        assert.equal(1, r.insertedCount);
        db.close();
        callback(docs);
    });
};

var findAllPhotographer = function(db, callback) {
    var collection = db.collection('photographers');
    collection.find({}).toArray(function(err, docs) {
        assert.equal(err, null);
        callback(docs);
    });
};

var findSinglePhotographer = (db, id, callback) => {
    var collection = db.collection('photographers');

    db.collection("photographers").findOne({'_id': id}, function(err, docs) {
        assert.equal(err, null);
        callback(docs);
      });
};

var updatePhotographer = (db, id, docs, callback) => {
    var collection = db.collection('photographers');

    collection.updateOne({'_id': id}, docs, (err, result) => {
        assert.equal(err, null);
        callback(result);
    });
};

var deletePhotographer = (db, id, callback) => {
    var collection = db.collection('photographers');

    collection.deleteOne({'_id': id},(err, docs) => {
        assert.equal(err, null);
        callback(docs);
    });
};

module.exports = {
    createPhotographer,
    findAllPhotographer,
    findSinglePhotographer,
    updatePhotographer,
    deletePhotographer
};