var assert = require('assert');

var insertFeedback = (db,task,callback)=> {
    var collection = db.collection('feedback');

    collection.insertOne(task,function (err,result) {
        assert.equal(err,null);
        //console.log("Inserted plus 1 to in the collection.");
        callback(result);

    })
}

var findFeedbacks = (db,callback)=> {

    var collection = db.collection('feedback');
    collection.find({}).toArray((err,docs)=>{
        assert.equal(err,null);
        callback(docs);
    })

}

var findFeedback = function (db,id,callback) {
    var collection = db.collection('feedback');
    collection.find(id).toArray((err,docs)=> {
        assert.equal(err,null);
        //console.log("Found the following records");
        //console.log(docs);
        callback(docs);

    })
}


var updateFeedback = (db,id,docs,callback)=> {

    var collection = db.collection('feedback');

    var myId = {
        '_id':id
    }

    collection.updateOne(myId, docs, (err,result)=> {
        assert.equal(err,null);

        callback(result);
    })


}

var removeFeedback =  (db,id,callback)=> {

    var collection = db.collection('feedback');

    var myId = {
        '_id':id
    }

    collection.deleteOne(myId,(err,result)=>{
        assert.equal(err,null);
        callback(result);
    })

}

module.exports = {
    insertFeedback,
    findFeedback,
    removeFeedback,
    findFeedbacks,
    updateFeedback
}