var assert = require('assert');

var createCategory = (db, docs, callback) => {
    
    db.collection('category').insertOne(docs, (err, r) => {
        assert.equal(null, err);
        db.close();
        callback(docs);
    });
};

var findAllCategory = function(db, callback) {
    var collection = db.collection('category');
    collection.find({}).toArray(function(err, docs) {
        assert.equal(err, null);
        callback(docs);
    });
};

var findSingleCategory = (db, id, callback) => {

    db.collection("category").findOne({'_id': id}, function(err, docs) {
        assert.equal(err, null);
        callback(docs);
      });
};

var updateCategory = (db, id, docs, callback) => {
    var collection = db.collection('category');

    collection.updateOne({'_id': id}, docs, (err, result) => {
        assert.equal(err, null);
        callback(result);
    });
};

var deleteCategory = (db, id, callback) => {
    var collection = db.collection('category');

    collection.deleteOne({'_id': id},(err, docs) => {
        assert.equal(err, null);
        callback(docs);
    });
};

module.exports = {
    createCategory,
    findAllCategory,
    findSingleCategory,
    updateCategory,
    deleteCategory
};