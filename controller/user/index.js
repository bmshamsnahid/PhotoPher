var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var configDB = require('../../config/database');
var userService = require('../../service/user');
var url = configDB.url;

var testControllerMethod = (req, res) => {
    res.status(200).json({"message": "Hello World from user controller!!!"});
};

var getAllUser = (req, res) => {
    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        userService.findAllUser(db, (docs) => {
            db.close();
            res.status(200).json(docs);
        });
    });
};

var getSingleUser = (req, res) => {
    var id = req.params.id;

    var ObjectID = require('mongodb').ObjectID;
    var o_id = new ObjectID(id);

    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        userService.findSingleUser(db, o_id, (docs) => {
            db.close();
            res.status(200).json(docs);
        });
    });
};

module.exports = {
    testControllerMethod,
    getAllUser,
    getSingleUser
};