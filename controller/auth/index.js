var passport = require('passport');

var testControllerMethod = (req, res) => {
    res.status(200).json({"message": "Hello World!!!"});
};

var userSignUp = passport.authenticate('local-signup', {
    successRedirect : '/auth/profile',
    failureRedirect : '/signup',
    failureFlash : true
});

var userLogin = passport.authenticate('local-login', {
    successRedirect : '/auth/profile',
    failureRedirect : '/login',
    failureFlash : true
});

var getUserProfile = (req, res) => {
    res.status(200).json(req.user);
};

var userLogout = (req, res) => {
    req.logout();
    res.status(200).json({
        'message': 'Logged out'
    });
};

var facebookAuth = passport.authenticate('facebook', { scope : 'email' });
var facebookCallback = passport.authenticate('facebook', {
    successRedirect : '/view/profile',
    failureRedirect : '/auth'
});

var googleAuth = passport.authenticate('google', { scope : ['profile', 'email'] });
var googleCallback = passport.authenticate('google', {
    successRedirect : '/auth/profile',
    failureRedirect : '/auth'
});

var twitterAuth = passport.authenticate('twitter', { scope : 'email' });
var twitterCallback = passport.authenticate('twitter', {
    successRedirect : '/auth/profile',
    failureRedirect : '/auth'
});

module.exports = {
    testControllerMethod,
    getUserProfile,
    userLogout,
    userLogin,
    userSignUp,
    facebookAuth,
    facebookCallback,
    googleAuth,
    googleCallback,
    twitterAuth,
    twitterCallback
};