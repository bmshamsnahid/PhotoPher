// server.js

// set up ======================================================================
// get all the tools we need
var express  = require('express');
var app      = express();
var path     = require('path');
var port     = process.env.PORT || 8080;
var mongoose = require('mongoose');
var passport = require('passport');
var flash    = require('connect-flash');

var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');

// router files ===============================================================
var authRoutes   = require('./routes/auth');
var userRoutes   = require('./routes/user');
var viewRoutes   = require('./routes/viewRoutes');
var feedbackRoutes = require('./routes/feedbackRoutes');
var messageRoutes = require('./routes/messageRoutes');
var photographerRoutes = require('./routes/photographerRoutes');
var photoRoutes = require('./routes/photoRoutes');
var categoryRoutes = require('./routes/categoryRoutes');

var configDB     = require('./config/database');

//for angular static files =====================================================
app.use(express.static(path.join(__dirname, 'public')));

// configuration ===============================================================
mongoose.connect(configDB.url); // connect to our database

//Enable cors supports ==========================================================
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
};
app.use(allowCrossDomain);

require('./config/passport')(passport); // pass passport for configuration

// set up our express application
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({ extended: true }));

app.set('view engine', 'ejs'); // set up ejs for templating

// required for passport
app.use(session({
    secret: 'eminem', // session secret
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

// routes ======================================================================
app.use('/auth', authRoutes);
app.use('/user', userRoutes);
app.use('/view', viewRoutes);
app.use('/feedback-routes', feedbackRoutes);
app.use('/message-routes', messageRoutes);
app.use('/photographer-routes', photographerRoutes);
app.use('/photo-routes', photoRoutes);
app.use('/category-routes', categoryRoutes);

// launch ======================================================================
app.listen(port);
console.log('The magic happens on port ' + port);
